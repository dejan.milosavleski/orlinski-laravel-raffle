/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./resources/js/app.js":
/*!*****************************!*\
  !*** ./resources/js/app.js ***!
  \*****************************/
/***/ (() => {

var stripe = Stripe('pk_test_51IVB82L9edRrwYWvCr4gu8IIxwHs1r4u56VI9HNbLkIpVL2j8JqFkulrVmZF3FcNN3rjP5hAQUnFT8Jv5j8GXbiu00fo21B8yj');
var elements = stripe.elements();
var card = elements.create('card', {
  hidePostalCode: true,
  style: {
    base: {
      iconColor: '#666EE8',
      color: '#31325F',
      lineHeight: '40px',
      fontWeight: 300,
      fontFamily: 'Helvetica Neue',
      fontSize: '15px',
      '::placeholder': {
        color: '#CFD7E0'
      }
    }
  }
});
card.mount('#card-element');

function setOutcome(result) {
  var successElement = document.querySelector('.success');
  var errorElement = document.querySelector('.error');
  successElement.classList.remove('visible');
  errorElement.classList.remove('visible');

  if (result.token) {
    // In this example, we're simply displaying the token
    successElement.querySelector('.token').textContent = result.token.id;
    successElement.classList.add('visible'); // In a real integration, you'd submit the form with the token to your backend server
    //var form = document.querySelector('form');
    //form.querySelector('input[name="token"]').setAttribute('value', result.token.id);
    //form.submit();
  } else if (result.error) {
    errorElement.textContent = result.error.message;
    errorElement.classList.add('visible');
  }
}

card.on('change', function (event) {
  setOutcome(event);
});
document.querySelector('form').addEventListener('submit', function (e) {
  e.preventDefault();
  var options = {
    name: document.getElementById('name').value,
    address_line1: document.getElementById('address-line1').value,
    address_line2: document.getElementById('address-line2').value,
    address_city: document.getElementById('address-city').value,
    address_state: document.getElementById('address-state').value,
    address_zip: document.getElementById('address-zip').value,
    address_country: document.getElementById('address-country').value
  };
  stripe.createToken(card, options).then(setOutcome);
});

/***/ }),

/***/ "./resources/css/app.css":
/*!*******************************!*\
  !*** ./resources/css/app.css ***!
  \*******************************/
/***/ (() => {

throw new Error("Module build failed (from ./node_modules/mini-css-extract-plugin/dist/loader.js):\nModuleBuildError: Module build failed (from ./node_modules/css-loader/dist/cjs.js):\nError: Can't resolve 'SFProText-SemiboldItalic.woff2' in '/Users/dejanmilosavleski/Documents/www/dev.orlinskishop.com/resources/css'\n    at finishWithoutResolve (/Users/dejanmilosavleski/Documents/www/dev.orlinskishop.com/node_modules/enhanced-resolve/lib/Resolver.js:293:18)\n    at /Users/dejanmilosavleski/Documents/www/dev.orlinskishop.com/node_modules/enhanced-resolve/lib/Resolver.js:362:15\n    at /Users/dejanmilosavleski/Documents/www/dev.orlinskishop.com/node_modules/enhanced-resolve/lib/Resolver.js:410:5\n    at eval (eval at create (/Users/dejanmilosavleski/Documents/www/dev.orlinskishop.com/node_modules/tapable/lib/HookCodeFactory.js:33:10), <anonymous>:16:1)\n    at /Users/dejanmilosavleski/Documents/www/dev.orlinskishop.com/node_modules/enhanced-resolve/lib/Resolver.js:410:5\n    at eval (eval at create (/Users/dejanmilosavleski/Documents/www/dev.orlinskishop.com/node_modules/tapable/lib/HookCodeFactory.js:33:10), <anonymous>:27:1)\n    at /Users/dejanmilosavleski/Documents/www/dev.orlinskishop.com/node_modules/enhanced-resolve/lib/DescriptionFilePlugin.js:87:43\n    at /Users/dejanmilosavleski/Documents/www/dev.orlinskishop.com/node_modules/enhanced-resolve/lib/Resolver.js:410:5\n    at eval (eval at create (/Users/dejanmilosavleski/Documents/www/dev.orlinskishop.com/node_modules/tapable/lib/HookCodeFactory.js:33:10), <anonymous>:15:1)\n    at /Users/dejanmilosavleski/Documents/www/dev.orlinskishop.com/node_modules/enhanced-resolve/lib/Resolver.js:410:5\n    at processResult (/Users/dejanmilosavleski/Documents/www/dev.orlinskishop.com/node_modules/webpack/lib/NormalModule.js:676:19)\n    at /Users/dejanmilosavleski/Documents/www/dev.orlinskishop.com/node_modules/webpack/lib/NormalModule.js:778:5\n    at /Users/dejanmilosavleski/Documents/www/dev.orlinskishop.com/node_modules/loader-runner/lib/LoaderRunner.js:399:11\n    at /Users/dejanmilosavleski/Documents/www/dev.orlinskishop.com/node_modules/loader-runner/lib/LoaderRunner.js:251:18\n    at context.callback (/Users/dejanmilosavleski/Documents/www/dev.orlinskishop.com/node_modules/loader-runner/lib/LoaderRunner.js:124:13)\n    at Object.loader (/Users/dejanmilosavleski/Documents/www/dev.orlinskishop.com/node_modules/css-loader/dist/index.js:154:5)\n    at processTicksAndRejections (internal/process/task_queues.js:93:5)");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	__webpack_modules__["./resources/js/app.js"]();
/******/ 	// This entry module doesn't tell about it's top-level declarations so it can't be inlined
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./resources/css/app.css"]();
/******/ 	
/******/ })()
;