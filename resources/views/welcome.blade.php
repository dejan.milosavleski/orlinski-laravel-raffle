<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Orlinskishop.com</title>
        <!-- Styles -->
        <style>
            /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */html{line-height:1.15;-webkit-text-size-adjust:100%}body{margin:0}a{background-color:transparent}[hidden]{display:none}html{font-family:system-ui,-apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji;line-height:1.5}*,:after,:before{box-sizing:border-box;border:0 solid #e2e8f0}a{color:inherit;text-decoration:inherit}svg,video{display:block;vertical-align:middle}video{max-width:100%;height:auto}.bg-white{--bg-opacity:1;background-color:#fff;background-color:rgba(255,255,255,var(--bg-opacity))}.bg-gray-100{--bg-opacity:1;background-color:#f7fafc;background-color:rgba(247,250,252,var(--bg-opacity))}.border-gray-200{--border-opacity:1;border-color:#edf2f7;border-color:rgba(237,242,247,var(--border-opacity))}.border-t{border-top-width:1px}.flex{display:flex}.grid{display:grid}.hidden{display:none}.items-center{align-items:center}.justify-center{justify-content:center}.font-semibold{font-weight:600}.h-5{height:1.25rem}.h-8{height:2rem}.h-16{height:4rem}.text-sm{font-size:.875rem}.text-lg{font-size:1.125rem}.leading-7{line-height:1.75rem}.mx-auto{margin-left:auto;margin-right:auto}.ml-1{margin-left:.25rem}.mt-2{margin-top:.5rem}.mr-2{margin-right:.5rem}.ml-2{margin-left:.5rem}.mt-4{margin-top:1rem}.ml-4{margin-left:1rem}.mt-8{margin-top:2rem}.ml-12{margin-left:3rem}.-mt-px{margin-top:-1px}.max-w-6xl{max-width:72rem}.min-h-screen{min-height:100vh}.overflow-hidden{overflow:hidden}.p-6{padding:1.5rem}.py-4{padding-top:1rem;padding-bottom:1rem}.px-6{padding-left:1.5rem;padding-right:1.5rem}.pt-8{padding-top:2rem}.fixed{position:fixed}.relative{position:relative}.top-0{top:0}.right-0{right:0}.shadow{box-shadow:0 1px 3px 0 rgba(0,0,0,.1),0 1px 2px 0 rgba(0,0,0,.06)}.text-center{text-align:center}.text-gray-200{--text-opacity:1;color:#edf2f7;color:rgba(237,242,247,var(--text-opacity))}.text-gray-300{--text-opacity:1;color:#e2e8f0;color:rgba(226,232,240,var(--text-opacity))}.text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}.text-gray-500{--text-opacity:1;color:#a0aec0;color:rgba(160,174,192,var(--text-opacity))}.text-gray-600{--text-opacity:1;color:#718096;color:rgba(113,128,150,var(--text-opacity))}.text-gray-700{--text-opacity:1;color:#4a5568;color:rgba(74,85,104,var(--text-opacity))}.text-gray-900{--text-opacity:1;color:#1a202c;color:rgba(26,32,44,var(--text-opacity))}.underline{text-decoration:underline}.antialiased{-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.w-5{width:1.25rem}.w-8{width:2rem}.w-auto{width:auto}.grid-cols-1{grid-template-columns:repeat(1,minmax(0,1fr))}@media (min-width:640px){.sm\:rounded-lg{border-radius:.5rem}.sm\:block{display:block}.sm\:items-center{align-items:center}.sm\:justify-start{justify-content:flex-start}.sm\:justify-between{justify-content:space-between}.sm\:h-20{height:5rem}.sm\:ml-0{margin-left:0}.sm\:px-6{padding-left:1.5rem;padding-right:1.5rem}.sm\:pt-0{padding-top:0}.sm\:text-left{text-align:left}.sm\:text-right{text-align:right}}@media (min-width:768px){.md\:border-t-0{border-top-width:0}.md\:border-l{border-left-width:1px}.md\:grid-cols-2{grid-template-columns:repeat(2,minmax(0,1fr))}}@media (min-width:1024px){.lg\:px-8{padding-left:2rem;padding-right:2rem}}@media (prefers-color-scheme:dark){.dark\:bg-gray-800{--bg-opacity:1;background-color:#2d3748;background-color:rgba(45,55,72,var(--bg-opacity))}.dark\:bg-gray-900{--bg-opacity:1;background-color:#1a202c;background-color:rgba(26,32,44,var(--bg-opacity))}.dark\:border-gray-700{--border-opacity:1;border-color:#4a5568;border-color:rgba(74,85,104,var(--border-opacity))}.dark\:text-white{--text-opacity:1;color:#fff;color:rgba(255,255,255,var(--text-opacity))}.dark\:text-gray-400{--text-opacity:1;color:#cbd5e0;color:rgba(203,213,224,var(--text-opacity))}}
        </style>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<link rel="stylesheet" href="{{ asset('css/app.css') }}" />
<!-- <script src="https://js.stripe.com/v3/"></script> -->
</head>
<body class="">
<div class="container-fluid px-0">
@if (Session::has('success'))
    @include('thankyou')
@else
    <!-- Marquee -->
    <div class="marquee"><div></div></div>
    <div class="text-center" id="logo-wrapper">
    <a class="navbar-brand" rel="home" href="/" title="Orlinskishop.com" itemprop="url">Orlinskishop.com</a>
    </div>
    <div class="container">
        <div class="row mx-0 align-items-start">
            <div class="col-md-12 col-xl-6 px-0 px-md-3">
                <div class="my-4 col-md-12 mx-auto">
                    <h4 class="text-primary text-uppercase mb-4">COMMENT CA MARCHE ?</h4>
                    <p>Pour vous inscrire dès maintenant et participer à la raffle, veuillez suivre les instructions ci-dessous :</p>
                    <div id="steps" class="my-4">
                        <div class="step active">
                            <h6 class="text-uppercase">PARTICIPATION </h6>
                            <p class="mb-4">Pour participer à la raffle, il suffit de remplir le formulaire ci-dessous. Vous avez jusqu’a 21h le dimanche 18 avril pour vous inscrire.</p>
                        </div>
                        <div class="step">
                            <h6 class="text-uppercase">RÉSULTATS DU TIRAGE AU SORT</h6>
                            <p class="mb-4">Le tirage au sort se fera le lundi 19 avril à partir de midi. Si vous faites partie des gagnants, vous recevrez un mail pour valider la commande de votre Roaring Lion Spirit (Petrol Edition) !</p>
                        </div>
                    </div>
                    <h6 class="text-uppercase">Veuillez noter:</h6>
                                            
                    <p>L’enregistrement ne garantit pas une place pour passer commande du <strong>Roaring Lion Spirit (Petrol Edition)</strong>.</p>
                    <p>Le tirage au sort ne tiendra pas compte de l’ordre d’inscription de chacun.</p>
                    <p>Attention, si vous avez été sélectionné, vous n’aurez que quelques heures pour passer commande. </p>
                    <p>Une fois le délai passé, vous ne pourrez plus passer commande du <strong>Roaring Lion Spirit (Petrol Edition)</strong>.</p>
                    <p><span style="color: #ff0000;"><strong>Attention, la raffle n’est pas un jeu concours.</strong><span><br>
                    </span><strong>Il s’agit d’un tirage au sort vous permettant d’avoir l’opportunité de passer commande et d’acheter le Roaring Lion Spirit (Petrol Edition) au prix de 159 €.</strong></span></p>

                </div>
            </div>
            <div class="col-md-12 col-xl-6 px-0 px-md-3">
                <div class="my-4 row mx-0" id="customer_login">
                    <div class="col-md-12 mx-auto">
                        <img src="/img/raffle-banner-roaring-lion.jpg" class="kong-silver img-fluid mb-3">
                        <p>Richard Orlinski est heureux de vous annoncer la sortie du <strong>Roaring Lion Spirit (Petrol Edition)</strong>!</p>
                        <p>Afin de donner les mêmes chances à tous d’acquérir le <strong>Roaring Lion Spirit</strong>, une raffle est organisée.</p>
                        <p>Vous trouverez toutes les informations ci-dessous.</p>
                        <p><strong>Attention</strong>, avant de valider votre inscription, pensez à bien vérifier vos informations et votre adresse mail. Vous ne pourrez plus les modifier une fois votre inscription validée.</p>
                        <h4 class="text-primary text-uppercase  mb-4">S’INSCRIRE À LA RAFFLE</h4>
                        <!-- <h3>La participation est maintenant terminée.</h3> -->
                       @include('raffle-register-form')
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endif
</div>


<script src="{{ asset('/js/app.js') }}"></script>

</body>
</html>
