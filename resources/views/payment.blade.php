<link rel="stylesheet" href="{{ asset('css/app.css') }}" />
<script src="https://js.stripe.com/v3/"></script>

<form action="{{url('charge')}}" method="POST">
    <input type="hidden" name="token" />
    <div class="group">
        <label>
            <span>Prénom</span>
            <input id="first_name" name="first_name" class="field" placeholder="Jane" />
        </label>
        <label>
            <span>Nom</span>
            <input id="last_name" name="last_name" class="field" placeholder="Jane" />
        </label>
    </div>
    <div class="group">
        <label>
            <span>Address</span>
            <input id="address-line1" name="address_line1" class="field" placeholder="77 Winchester Lane" />
        </label>
        <label>
            <span>Address (cont.)</span>
            <input id="address-line2" name="address_line2" class="field" placeholder="" />
        </label>
        <label>
            <span>City</span>
            <input id="address-city" name="address_city" class="field" placeholder="Coachella" />
        </label>
        <label>
            <span>State</span>
            <input id="address-state" name="address_state" class="field" placeholder="CA" />
        </label>
        <label>
            <span>ZIP</span>
            <input id="address-zip" name="address_zip" class="field" placeholder="92236" />
        </label>
        <label>
            <span>Country</span>
            <input id="address-country" name="address_country" class="field" placeholder="United States" />
        </label>
    </div>
    <div class="group">
        <label>
            <span>Card</span>
            <div id="card-element" class="field"></div>
        </label>
    </div>
    <button type="submit">Pay $25</button>
    <div class="outcome">
        <div class="error"></div>
        <div class="success">
            Success! Your Stripe token is <span class="token"></span>
        </div>
    </div>
</form>

<script src="{{ asset('/js/app.js') }}"></script>
