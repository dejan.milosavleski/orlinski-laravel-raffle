<form action="" method="POST">
        @csrf

        <div class="group">
        <label>
            <span>Civilite</span>
            <select name="civility" class="field">
                <option value="mr">Mr</option>
                <option value="mrs">Mme</option>
            </select>
        </label>
        <label>
            <span>Prénom</span>
            <input id="first_name" name="first_name" class="field" placeholder="Jane" required/>
        </label>
        <label>
            <span>Nom</span>
            <input id="last_name" name="last_name" class="field" placeholder="Jane" required/>
        </label>
             <label>
                <span>Age</span>
                <select name="age" class="field">
                <option value="0">Votre âge</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option><option value="61">61</option><option value="62">62</option><option value="63">63</option><option value="64">64</option><option value="65">65</option><option value="66">66</option><option value="67">67</option><option value="68">68</option><option value="69">69</option><option value="70">70</option><option value="71">71</option><option value="72">72</option><option value="73">73</option><option value="74">74</option><option value="75">75</option><option value="76">76</option><option value="77">77</option><option value="78">78</option><option value="79">79</option><option value="80">80</option><option value="81">81</option><option value="82">82</option><option value="83">83</option><option value="84">84</option><option value="85">85</option><option value="86">86</option><option value="87">87</option><option value="88">88</option><option value="89">89</option><option value="90">90</option><option value="91">91</option><option value="92">92</option><option value="93">93</option><option value="94">94</option><option value="95">95</option><option value="96">96</option><option value="97">97</option><option value="98">98</option><option value="99">99</option>
                </select>
            </label>       
            <label>
                <span>Adresse</span>
                <input id="address-line" name="address_line" class="field " placeholder="77 Winchester Lane" required/>
            </label>

            <label>
                <span>City</span>
                <input id="address-city" name="address_city" class="field " placeholder="Coachella" required/>
            </label>
            <label>
                <span>State</span>
                <input id="address-state" name="address_state" class="field " placeholder="CA" required/>
            </label>
            <label>
                <span>ZIP</span>
                <input id="address-zip" name="address_zip" class="field " placeholder="92236" required/>
            </label>
            <label>
                <span>Pays</span>
                <input id="address-country" name="address_country" class="field " placeholder="United States" required/>
            </label>

            <label>
                <span>Telephone</span>
                <input id="phone_number" name="phone_number" class="field " placeholder="+1 23424" required/>
            </label>
            <label>
                <span>Votre email</span>
                <input id="email" name="email" type="email" class="field " placeholder="United States" required/>
            </label>
        </div>

        <button type="submit">Register a la raffle</button>
        <div class="outcome">
            <div class="error">E-mail already exists</div>

        </div>
    </form>