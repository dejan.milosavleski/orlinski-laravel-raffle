<?php

namespace App\Http\Controllers;

use App\Models\Registration;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Juanparati\Sendinblue\Facades\Template;
class RegistrationController extends Controller
{
    public function save(Request $request){
        $registration = new Registration();
        $registration->civility = $request->civility;
        $registration->first_name = $request->first_name;
        $registration->last_name = $request->last_name;
        $registration->age = $request->age;
        $registration->address_line = $request->address_line;
        $registration->address_city = $request->address_city;
        $registration->address_state = $request->address_state;
        $registration->address_zip = $request->address_zip;
        $registration->address_country = $request->address_country;
        $registration->phone_number = $request->phone_number;
        $registration->email = $request->email;
        $registration->save();
        Template::to($request->email);
        Template::send(121);
        Redirect::back()->withSuccess("Thank you!!!!");
    }
}
